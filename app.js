const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port,()=>{
    console.log(`Projeto executado na porta ${port}`);
});

app.get('/aluno',(req, res)=>{
    res.send(`{message: aluno found}`);
});

// recurso de request.query
app.get('/aluno/filtro',(req, res)=>{
    let source = req.query;
    let ret = `Dados solicitados: ${source.nome} ${source.sobrenome}.`;
    res.send(`{message: ${ret}}`);
});

// recurso de request.param
app.get('/aluno/filtro/:valor',(req, res)=>{
    let dado = req.params.valor;
    let ret = `Dado solicitado: ${dado}`;
    res.send(`{message: ${ret}}`);
});

// recurso de request.body
app.post('/aluno',(req, res)=>{
    let dados = req.body;
    let ret = `Dados enviados: ${dados.nome} `;
    ret += `Sobrenome: ${dados.sobrenome} `;
    ret += `Idade: ${dados.idade}.`;
    res.send(`{message: ${ret}}`);
});

